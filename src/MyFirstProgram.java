public class MyFirstProgram {
    public static void main(String[] args)
    {
        System.out.println("Hello world!");
        System.out.println("next comment");
        System.out.println("my favorite food :");
        System.out.println("1. Bakso");
        System.out.println("2. Nasi Goreng");
        //memanggil metod
        myFavoriteFood();
        myFavoriteMaterial();
        myFavoritePlace();
        myFavoriteFriends();
        myFavoriteHobby();
    }

    //ini metod favorite food
    public static void myFavoriteFood(){
        System.out.println("my favorite food :");
        System.out.println("1. Bakso");
        System.out.println("2. Nasi Goreng");
    }

    //ini metod favorite place
    public static void myFavoritePlace(){
        System.out.println("my favorite place :");
        System.out.println("1. Ancol");
        System.out.println("2. Cafe Pendekar Deket Rumah");
    }

    //ini metod favorite place
    public static void myFavoriteFriends(){
        System.out.println("my favorite friens :");
        System.out.println("1. Budi");
        System.out.println("2. Dipa");
        System.out.println("3. Padu");
    }

    //ini metod favorite place
    public static void myFavoriteMaterial(){
        System.out.println("my favorite material :");
        System.out.println("1. A");
        System.out.println("2. B");
        System.out.println("3. C");
    }

    //ini metod favorite place
    public static void myFavoriteHobby(){
        System.out.println("my favorite hobby :");
        System.out.println("1. Lari");
        System.out.println("2. Makan");
        System.out.println("3. Staycation");
    }

}