import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class Looping {
    public static void main(String[] args) {
        sample2(10);
        sample3(8);
        sample4();
    }

    public static void sample1(int n){
        for (int i=0; i<=n; i++){
            System.out.println(i);
        }
    }

    public static void sample2(int n){
        for (int i=0; i<=n; i++){
            if(i % 2 ==0){
                System.out.print("Fizz ");
            }else {
                System.out.print(i + "\t");
            }
        }
    }

    public static void sample3(int n){
        System.out.println("\n");
        for (int i=0; i < n; i++){
            for (int j=0; j < n; j++){
                System.out.print("["+i+","+j+"]\t");
            }
            System.out.println("\n");
        }
    }

    public static void sample4(){
        List<String> listString = Arrays.asList("Coding", "Reading", "Swimming");
        System.out.println("List of Hobby");
        for(String item : listString){
            System.out.print(item + "\t");
        }

        List<Integer> listNumber = Arrays.asList(2,6,7,11,9);
        System.out.println("List of Number");
        for(Integer item : listNumber){
            System.out.print(item + "\t");
        }
    }
}
